package pl.sdaacademy.rubiksCube;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;


public class Controller {
    Canvas[] fields = new Canvas[6];;
    GraphicsContext graphicsContext2D;
    int POINT_SIZE = 40;
    RubiksCube rubiksCube;
    Button[] buttons = new Button[14];
    Canvas canvas;
    @FXML
    Button RandomCubeButton;
    @FXML
    HBox hBox;
    @FXML
    GridPane gridPane2;
    @FXML
    private GridPane gridPane;
    @FXML
    private Button downButton1;
    @FXML
    private Button downButton2;
    @FXML
    private Button downButton3;
    @FXML
    private Button upButton1;
    @FXML
    private Button upButton2;
    @FXML
    private Button upButton3;
    @FXML
    private Button leftButton1;
    @FXML
    private Button leftButton2;
    @FXML
    private Button leftButton3;
    @FXML
    private Button rightButton1;
    @FXML
    private Button rightButton2;
    @FXML
    private Button rightButton3;
    @FXML
    private Button turnLeftButton;
    @FXML
    private Button turnRightButton;
    @FXML
    Button solveButton;
    @FXML
    Button previousButton;

    public void initialize() {
        rubiksCube = new RubiksCube();
        for (int i = 0; i < 6; i++) {

            Canvas canvas1 = new Canvas();
            canvas1.setHeight(140);
            canvas1.setWidth(140);
            fields[i] = canvas1;
        }
        gridPane.add(fields[0], 1, 2);
        gridPane.add(fields[1], 1, 3);
        gridPane.add(fields[2], 0, 2);
        gridPane.add(fields[3], 2, 2);
        gridPane.add(fields[4], 1, 1);
        gridPane.add(fields[5], 1, 0);
        gridPane.setGridLinesVisible(false);

             canvas = new Canvas();
            canvas.setWidth(300);
            canvas.setHeight(300);
            gridPane2.add(canvas, 0, 1);



        rubiksCube.setPaintStrategy(i -> {

            if (i < 6) {
                graphicsContext2D = fields[i].getGraphicsContext2D();
                drawSideOfSquare(i, 0);
            } else if (i >= 6) {
                graphicsContext2D = canvas.getGraphicsContext2D();
                drawSideOfSquare(0, 2);
                drawSideIn3D(3);
                drawTopIn3D(4);

            }
        });
        rubiksCube.show();
        buttons[0] = upButton1;
        buttons[1] = upButton2;
        buttons[2] = upButton3;
        buttons[3] = downButton1;
        buttons[4] = downButton2;
        buttons[5] = downButton3;
        buttons[6] = leftButton1;
        buttons[7] = leftButton2;
        buttons[8] = leftButton3;
        buttons[9] = rightButton1;
        buttons[10] = rightButton2;
        buttons[11] = rightButton3;
        buttons[12] = turnLeftButton;
        buttons[13] = turnRightButton;
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            buttons[i].setOnAction(event -> {
                rubiksCube.turnColumn(finalI, VerticallDirection.UP);
                rubiksCube.show();
                rubiksCube.addMove(2, finalI);
            });
        }
        for (int i = 3; i < 6; i++) {
            int finalI = i - 3;
            buttons[i].setOnAction(event -> {
                rubiksCube.turnColumn(finalI, VerticallDirection.DOWN);
                rubiksCube.show();
                rubiksCube.addMove(3, finalI);
            });
        }
        for (int j = 6; j < 9; j++) {
            int finalI1 = j - 6;
            buttons[j].setOnAction(event -> {
                rubiksCube.turnRow(finalI1, HorizontalyDierction.LEFT);
                rubiksCube.show();
                rubiksCube.addMove(0, finalI1);
            });
        }
        for (int i = 9; i < 12; i++) {
            int finalI = i - 9;
            buttons[i].setOnAction(event -> {
                rubiksCube.turnRow(finalI, HorizontalyDierction.RIGHT);
                rubiksCube.show();
                rubiksCube.addMove(1,  finalI);
            });
        }
        buttons[12].setOnAction(event -> {
            rubiksCube.turnCube(HorizontalyDierction.LEFT);
            rubiksCube.show();
            rubiksCube.addMove(5, 0);
        });
        buttons[13].setOnAction(event -> {
            rubiksCube.turnCube(HorizontalyDierction.RIGHT);
            rubiksCube.show();
            rubiksCube.addMove(4, 0);
        });
        RandomCubeButton.setOnAction(event -> {
            rubiksCube.randomCube();
            rubiksCube.show();
        });

solveButton.setOnAction(event -> {
    rubiksCube.solve();
    rubiksCube.show();
});
previousButton.setOnAction(event -> {
    rubiksCube.previous();
    rubiksCube.show();
});
    }

    private void drawTopIn3D(int i) {
        int[][] top = rubiksCube.getCube()[i];
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                Color color = takeColor(top[j][k]);
                drawDiamondOnTop(j, k, color);
            }

        }
    }

    private void drawDiamondOnTop(int j, int k, Color color) {
        int x=75+j*40-k*25;
        int y =5+k*25;
        double[] X = new  double[4];
        double[] Y = new  double[4];

        X[0]=(double)x;
        X[1]=(double)x+40;
        X[2]=(double)x+15;
        X[3]=(double)x-25;
        Y[0]=(double)y;
        Y[1]=(double)y;
        Y[2]=(double)y+25;
        Y[3]=(double)y+25;
        graphicsContext2D.setFill(Color.BLACK);
        graphicsContext2D.fillPolygon(X, Y, 4);
        X[0]=(double)x+1;
        X[1]=(double)x+35;
        X[2]=(double)x+14;
        X[3]=(double)x-20;
        Y[0]=(double)y+2;
        Y[1]=(double)y+2;
        Y[2]=(double)y+23;
        Y[3]=(double)y+23;
        graphicsContext2D.setFill(color);
        graphicsContext2D.fillPolygon(X, Y, 4);
    }


    private void drawPoint(int x, int y, Color color) {
        graphicsContext2D.setFill(Color.BLACK);
        graphicsContext2D.fillRect(x * POINT_SIZE, y * POINT_SIZE, POINT_SIZE, POINT_SIZE);
        graphicsContext2D.setFill(color);
        graphicsContext2D.fillRect(x * POINT_SIZE + 2, y * POINT_SIZE + 2, POINT_SIZE - 4, POINT_SIZE - 4);
    }

    public void drawSideOfSquare(int i, int movement) {
        int[][] square = rubiksCube.getCube()[i];
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                int numberOfColor = square[j][k];
                Color color = takeColor(numberOfColor);
                drawPoint(j, k+movement, color);

            }

    }
    }

    private Color takeColor(int numberColor) {
        switch (numberColor) {
            case 0:
                return Color.GREEN;
            case 1:
                return Color.RED;
            case 2:
                return Color.BLUE;
            case 3:
                return Color.YELLOW;
            case 4:
                return Color.PURPLE;
            default:
                return Color.BROWN;

        }
    }
    public  void  drawSideIn3D(int i) {
        int[][] side = rubiksCube.getCube()[i];
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                Color color = takeColor(side[j][k]);
                drawDiamondOnSide(j, k, color);

            }

        }

    }

    private void drawDiamondOnSide(int j, int k, Color color) {
    double[] X = new  double[4];
    double[] Y = new  double[4];

        int x=120+j*25;
        int y=80-j*25+k*40;

        X[0]=(double)x;
        X[1]=(double)x+25;
        X[2]=(double)x+25;
        X[3]=(double)x;
        Y[0]=(double)y;
        Y[1]=(double)y-25;
        Y[2]=(double)y+15;
        Y[3]=(double)y+40;
        graphicsContext2D.setFill(Color.BLACK);
        graphicsContext2D.fillPolygon(X, Y, 4);
        X[0]=(double)x+2;
        X[1]=(double)x+23;
        X[2]=(double)x+23;
        X[3]=(double)x+2;
        Y[0]=(double)y+1;
        Y[1]=(double)y-20;
        Y[2]=(double)y+14;
        Y[3]=(double)y+35;
        graphicsContext2D.setFill(color);
        graphicsContext2D.fillPolygon(X, Y, 4);
    }
}



