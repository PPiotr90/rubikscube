package pl.sdaacademy.rubiksCube;

public interface PaintStrategy {
    public  void paint(int i);
}
