package pl.sdaacademy.rubiksCube;

import java.util.List;
import java.util.Random;

public class RubiksCube {
    private int[][][] cube = new int[6][3][3];
    private List<Integer>[] availableColors = new List[9];
    private PaintStrategy paintStrategy;
    private String moves;

    public RubiksCube() {
        moves = new String();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {

                    cube[i][j][k] = i;
                }
            }

        }
    }

    public void turnColumn(int i, VerticallDirection verticallDirection) {
        switch (verticallDirection) {
            case UP:
                int[] temp = cube[0][i];
                cube[0][i] = cube[1][i];
                cube[1][i] = cube[5][i];
                cube[5][i] = cube[4][i];
                cube[4][i] = temp;
                if (i == 0) {
                    swapRowsColumns(2, HorizontalyDierction.LEFT);
                } else if (i == 2) {
                    swapRowsColumns(3, HorizontalyDierction.RIGHT);
                }

                break;
            case DOWN:
                turnColumn(i, VerticallDirection.UP);
                turnColumn(i, VerticallDirection.UP);
                turnColumn(i, VerticallDirection.UP);

                break;
        }

    }

    public void turnRow(int k, HorizontalyDierction horizontalyDierction) {
        switch (horizontalyDierction) {

            case RIGHT:
                for (int j = 0; j < 3; j++) {
                    int temp = cube[0][j][k];
                    cube[0][j][k] = cube[2][j][k];
                    cube[2][j][k] = cube[5][j][change(k)];
                    cube[5][j][change(k)] = cube[3][j][k];
                    cube[3][j][k] = temp;
                    if (k == 0) {
                        swapRowsColumns(4, HorizontalyDierction.RIGHT);
                    } else if ((k == 2)) {
                        swapRowsColumns(1, HorizontalyDierction.LEFT);
                    }

                }

                break;
            case LEFT:
                turnRow(k, HorizontalyDierction.RIGHT);
                turnRow(k, HorizontalyDierction.RIGHT);
                turnRow(k, HorizontalyDierction.RIGHT);

                break;
        }

    }

    public void turnCube(HorizontalyDierction horizontalyDierction) {
        switch (horizontalyDierction) {
            case RIGHT:
                for (int i = 0; i < 5; i++) {
                    swapRowsColumns(i, horizontalyDierction);
                }
                swapRowsColumns(5, HorizontalyDierction.LEFT);
                int[][] temp = cube[1];
                cube[1] = cube[3];
                cube[3] = cube[4];
                cube[4] = cube[2];
                cube[2] = temp;

                break;
            case LEFT:
                turnCube(HorizontalyDierction.RIGHT);
                turnCube(HorizontalyDierction.RIGHT);
                turnCube(HorizontalyDierction.RIGHT);

                break;
        }

    }

    private int change(int i) {
        int result = 1;
        if (i == 0) {
            result = 2;
        } else if (i == 2) {
            result = 0;
        }
        return result;
    }

    public void randomCube() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int action = (int) (random.nextDouble() * 6);
            int position = (int) (random.nextDouble() * 3);
            moves += String.valueOf(action) + "," + String.valueOf(position) + ";";
            switch (action) {
                case 0:
                    turnRow(position, HorizontalyDierction.LEFT);
                    break;
                case 1:
                    turnRow(position, HorizontalyDierction.RIGHT);
                    break;
                case 2:
                    turnColumn(position, VerticallDirection.UP);
                    break;
                case 3:
                    turnColumn(position, VerticallDirection.DOWN);
                    break;
                case 4:
                    turnCube(HorizontalyDierction.RIGHT);
                    break;
                default:
                    turnCube(HorizontalyDierction.LEFT);
                    break;
            }
        }
    }

    public void setPaintStrategy(PaintStrategy paintStrategy) {
        this.paintStrategy = paintStrategy;
    }

    public int[][][] getCube() {
        return cube;
    }

    public void show() {
        for (int i = 0; i < 7; i++) {
            paintStrategy.paint(i);
        }
    }

    private void swapRowsColumns(int i, HorizontalyDierction horizontalyDierction) {
        switch (horizontalyDierction) {
            case RIGHT:
                int[][] temp = new int[3][3];
                for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < 3; k++) {
                        temp[j][k] = cube[i][j][k];
                    }
                }
                for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < 3; k++) {
                        cube[i][change(k)][j] = temp[j][k];

                    }

                }
                break;
            case LEFT:
                swapRowsColumns(i, HorizontalyDierction.RIGHT);
                swapRowsColumns(i, HorizontalyDierction.RIGHT);
                swapRowsColumns(i, HorizontalyDierction.RIGHT);
                break;

        }
    }

    public void solve() {
        while (!(moves.equals(new String()))) {
            previous();
        }
    }

    public void addMove(int action, int position) {
        String s = String.valueOf(action) + "," + String.valueOf(position) + ";";
        moves += s;

    }

    public void previous() {
        if (!(moves.equals(new String()))) {
            String move = moves.substring(moves.length() - 4, moves.length() - 1);
            String[] oneMove = move.split(",");
            int action = Integer.parseInt(oneMove[0]);
            int position = Integer.parseInt(oneMove[1]);
            switch (action) {
                case 1:
                    turnRow(position, HorizontalyDierction.LEFT);
                    break;
                case 0:
                    turnRow(position, HorizontalyDierction.RIGHT);
                    break;
                case 3:
                    turnColumn(position, VerticallDirection.UP);
                    break;
                case 2:
                    turnColumn(position, VerticallDirection.DOWN);
                    break;
                case 4:
                    turnCube(HorizontalyDierction.LEFT);
                    break;
                default:
                    turnCube(HorizontalyDierction.RIGHT);
                    break;
            }
            if (moves.length() - 4 < 0) moves = new String();
            moves = moves.substring(0, moves.length() - 4);
        }
    }
}
