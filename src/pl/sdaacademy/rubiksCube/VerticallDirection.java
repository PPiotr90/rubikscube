package pl.sdaacademy.rubiksCube;

public enum VerticallDirection  implements  Direction{
    UP, DOWN
}
